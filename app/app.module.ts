import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { LoadGameService } from './services/load/load-game.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ErrorComponent } from './components/error/error.component';
import { AboutComponent } from './components/about/about.component';
import { BrowserComponent } from './components/play/browser/browser.component';
import { BoardComponent } from './components/play/board/board.component';
import { GameComponent } from './components/play/game/game.component';
import { CardComponent } from './components/play/card/card.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/play', pathMatch: 'full' },
  { path: 'play', component: BrowserComponent },
  { path: 'play/:id', component: GameComponent },
  { path: 'about', component: AboutComponent }
]

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  declarations: [
    AppComponent,
    NavbarComponent,
    ErrorComponent,
    AboutComponent,
    BrowserComponent,
    GameComponent,
    BoardComponent,
    CardComponent
  ],
  providers: [LoadGameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
