import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'mmz-error',
    templateUrl: 'error.component.html'
})
export class ErrorComponent {
    @Input() error: Error;
}