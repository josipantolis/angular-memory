import { Component } from '@angular/core';
import { LoadGameService, GameDescription } from '../../../services/load/load-game.service';

@Component({
    moduleId: module.id,
    templateUrl: 'browser.component.html'
})
export class BrowserComponent {
    games: GameDescription[] = [];
    error: Error = null;

    constructor(private loadService: LoadGameService) {
        loadService.loadGamesList()
            .subscribe(games => this.games = games, error => this.error = error);
    }
}