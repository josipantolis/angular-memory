import { Component, EventEmitter, Input, Output, ViewChildren, QueryList } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { UICard } from '../game/game.component';

@Component({
    moduleId: module.id,
    selector: 'mmz-board',
    templateUrl: 'board.component.html'
})
export class BoardComponent {
    @Output() cardSelected = new EventEmitter<UICard>();
    @ViewChildren(CardComponent) cardComponents: QueryList<CardComponent>;
    cardRows: UICard[][];

    @Input()
    set cards(cards: UICard[]) {
        this.initiateGameWith(cards);
    }

    onCardClick(clickedCard: UICard) {
        this.cardSelected.emit(clickedCard);
    }

    hide(cards: UICard[]) {
        const idsToHide = cards.map(card => card.id);
        this.cardComponents
            .filter(comp => idsToHide.indexOf(comp.id) >= 0)
            .forEach(comp => comp.hide());
    }

    restartGameWith(cards: UICard[]) {
        this.initiateGameWith(cards);
    }

    private initiateGameWith(cards: UICard[]) {
        const shuffledCards = this.shuffle(cards);
        this.cardRows = [];
        let i: number;
        for (i = 0; i < shuffledCards.length; i += 3) {
            this.cardRows.push(shuffledCards.slice(i, i + 3));
        }
    }

    private shuffle<T>(array: T[]): T[] {
        let index = array.length, switchWithIndex: number, temp: T;
        while (index) {
            switchWithIndex = Math.floor(Math.random() * index--);
            temp = array[index];
            array[index] = array[switchWithIndex];
            array[switchWithIndex] = temp;
        }
        return array;
    }
}