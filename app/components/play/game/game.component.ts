import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import { LoadGameService, Game, Card } from '../../../services/load/load-game.service';
import { BoardComponent } from '../board/board.component';

@Component({
    moduleId: module.id,
    templateUrl: 'game.component.html'
})
export class GameComponent implements OnInit, OnDestroy {
    game: Game;
    cards: UICard[];
    error: Error;

    private selectedCards: UICard[] = [];
    private matchedCards: UICard[] = [];
    private gameSubscription: Subscription;
    @ViewChild(BoardComponent) private boardComponent: BoardComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private loadService: LoadGameService
    ) { }

    ngOnInit() {
        this.gameSubscription = this.route.params
            .map(params => params['id'])
            .switchMap(id => this.loadService.loadGame(id))
            .subscribe(game => {
                this.game = game;
                this.cards = game.cards.map((card, index) => {
                    const uiComponents = {
                        id: index,
                        backUrl: game.board.backImageUrl
                    };
                    return Object.assign({}, card, uiComponents);
                });
            }, error => this.error = error);
    }

    ngOnDestroy() {
        this.gameSubscription.unsubscribe();
    }

    onCardSelected(selectedCard: UICard) {
        if (this.isAlreadySelectedOrMatched(selectedCard)) {
            return;
        }
        if (this.selectedEnoguhCardsToMatch()) {
            this.boardComponent.hide(this.selectedCards);
            this.selectedCards = [];
        }
        this.selectedCards.push(selectedCard);
        if (this.selectedEnoguhCardsToMatch()) {
            if (this.allSelectedCardsMathcEachOther()) {
                this.selectedCards.forEach(card => this.matchedCards.push(card));
                this.selectedCards = [];
            }
            if (this.victoryIsAchieved()) {
                this.onVictory();
                return;
            }
        }
    }

    private isAlreadySelectedOrMatched(card: UICard): boolean {
        if(this.selectedCards.indexOf(card) !== -1) {
            return true;
        }
        if(this.matchedCards.indexOf(card) !== -1) {
            return true;
        }
        return false;
    }

    private selectedEnoguhCardsToMatch(): boolean {
        return this.selectedCards.length === this.game.board.cardsPerGroup;
    }

    private allSelectedCardsMathcEachOther(): boolean {
        return !!this.selectedCards
            .map(card => card.group)
            .reduce((groupA, groupB) => groupA === groupB ? groupA : null);
    }

    private victoryIsAchieved(): boolean {
        return this.matchedCards.length === this.cards.length;
    }

    private onVictory() {
        setTimeout(() => {
            if (confirm('You win! Congradulations! Up for another round?')) {
                this.matchedCards = [];
                this.selectedCards = [];
                this.boardComponent.restartGameWith(this.cards);
            } else {
                this.router.navigate(['/play']);
            }
        }, 500);
    }
}

export interface UICard extends Card {
    id: number;
    backUrl: string;
}
