import { Component, Input, 
    ViewChild, ElementRef 
} from '@angular/core';
import { UICard } from '../game/game.component';
import { Audio } from '../../../services/load/load-game.service';

@Component({
    moduleId: module.id,
    selector: 'mmz-card',
    templateUrl: 'card.component.html',
    styleUrls: ['card.component.css']
})
export class CardComponent {
    flipClass = '';
    id: number;
    text: string;
    imageUrl: string;
    audio: Audio;
    group: number;
    backImageUrl: string;
    @ViewChild('audioplayer') audioElement: ElementRef;

    @Input()
    set card(card: UICard) {
        this.text = card.text;
        this.imageUrl = card.imageUrl;
        this.audio = card.audio;
        this.group = card.group;
        this.backImageUrl = card.backUrl;
        this.id = card.id;
    }

    onSelect() {
        this.flipClass = 'flipped';
    }

    hide() {
        this.flipClass = '';
        if (this.audioElement && this.audioElement.nativeElement) {
            this.audioElement.nativeElement.pause();
        }
    }

}