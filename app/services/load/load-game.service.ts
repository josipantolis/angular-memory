import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class LoadGameService {
    private jsonStoreUrl = 'https://api.myjson.com/bins/';
    private allGamesId = '142rlb';

    constructor(private http: Http) { }

    loadGamesList(): Observable<GameDescription[]> {
        const gamesListUrl = this.jsonStoreUrl + this.allGamesId;
        return this.http.get(gamesListUrl)
            .map(result => result.json())
            .map(resultJson => resultJson.games)
            .map(games => games.filter((game: GameDescription) => game.isPublic));
    }

    loadGame(gameId: string): Observable<Game> {
        const gameUrl = this.jsonStoreUrl + gameId;
        return this.http.get(gameUrl)
            .map(result => result.json());
    }
}

export interface GameDescription {
    id: string;
    title: string;
    description: string;
    isPublic: boolean;
}

export interface Game {
    title: string;
    description: string;
    board: BoardConfig;
    cards: Card[];
}

export interface BoardConfig {
    width: number;
    height: number;
    cardsPerGroup: number;
    backImageUrl: string;
}

export interface Card {
    group: number;
    text: string;
    imageUrl: string;
    audio: Audio;
}

export interface Audio {
    url: string;
    type: string;
}