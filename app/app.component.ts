import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'mmz-app',
  templateUrl: 'app.component.html'
})
export class AppComponent {  }
